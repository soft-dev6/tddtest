/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.proceduralox;

import java.util.Scanner;

/**
 *
 * @author hanam
 */
public class OX {
    
    static char[][] board = {{'-', '-', '-',}, {'-', '-', '-',}, {'-', '-', '-',}};
    static char currentPlayer = 'O';
    static int row,col;
    static boolean Game = false;
    static Scanner kb = new Scanner(System.in);
    static int mess =0;
    public static void main(String[] args) {
        
         printWelcome();
        while (true) {
            showBoard();
            printOorX(currentPlayer);
            printRowCol();
           calculate();
            if (Game) {
                break;
            }
        }
         
    }

    public static boolean setPosition() {
        board[row-1][col-1] = currentPlayer;
        return true;
    }

    public static void switchPlayer() {
        if(currentPlayer!='X') currentPlayer = 'X';
        else
            currentPlayer='O';
    }
    
    public static boolean checkWin(){
        if(checkVertical(board,currentPlayer,col)){
            return true;
        }else if(checkHorizontal()){
            return true;
        }else if(checkDigonal1()){
             return true;
        }else if(checkDigonal2()){
            return true;
        }
        return false;
    }
    
    public static void calculate(){
        if(setPosition()){
            if(checkWin()){
                showBoard();
                printWin();
                Game =  true;
                return;
            }
            if(checkDraw()){
                showBoard();
                printDraw();
                Game =  true;
                return;
            }
            mess++;
            switchPlayer();
        }
    } 

    public static void printDraw() {
        System.out.println(">>>Draw<<<");
    }

    public static void printWin() {
        System.out.println(">>>"+ currentPlayer +" Win<<<");
    }
    public static void printRowCol() {
        System.out.println("Please input row, col:");
        choosePosition();
    }

    public static void choosePosition() {
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void printOorX(char currentPlayer) {
        System.out.println("Turn " + currentPlayer);
    }

    public static void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static boolean checkVertical(char[][] board, char currentPlayer,int col) {
        for(int r =0;r<board.length;r++){ //r = row
            if(board[r][col-1] != currentPlayer) return false;
        }
        return true;
    }

    public static boolean checkHorizontal() { // c = col
        for(int c = 0; c< board.length;c++){
            if(board[row-1][c]!=currentPlayer) return false;
        }
        return true;
    }

    public static boolean checkDigonal1() { //11 22 33
        for(int i = 0; i< board.length;i++){
            if(board[i][i]!=currentPlayer)return false;
        }
        return true;
    }
    
    public static boolean checkDigonal2() { //13 22 31
        for(int i = 0; i< board.length;i++){
            if(board[i][2-i]!=currentPlayer) return false;
        }
        return true;
    }

    public static boolean checkDraw() {
         if(mess==8){
             return true;
         }
         return false;
    }
    
}
